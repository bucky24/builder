const express = require("express");
//const Builder = require('@bucky24/builder');
const Builder = require('../../src/index');
const path = require('path');

const port = process.env.PORT || 8080;

const app = express();

const builder = new Builder(path.join(__dirname, 'webpack.config.js'));

app.get('/', async (req, res) => {
    builder.serveIndex(res);
});

app.get('/route', async (req, res) => {
    builder.serveIndex(res);
});

app.get('/anotherroute', async (req, res) => {
    builder.serveIndex(res);
});

// any additional routes go here

app.get("*", (req, res) => {
    builder.serveStatic(req, res);
});

const server = app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

builder.setServer(server);
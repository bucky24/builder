import React from 'react';

import styles from './styles.css';

export default function App() {
	return (<div className={styles.appRoot}>
		Welcome to @bucky24/builder
	</div>);
}
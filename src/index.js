const webpack = require('webpack');
const fs = require('fs');
const path = require('path');
const ws = require('ws');

function sendFile(res, file) {
    res.contentType(path.basename(file));
    fs.createReadStream(file).pipe(res);
}

class Builder {
    constructor(config) {
        this.config = require(config);
        this.compiler = webpack(this.config);

        this.wsServer = new ws.Server({ noServer: true });

        this.compiler.watch({
            aggregateTimeout: 300,
            poll: undefined
        }, (err, stats) => {
            if (stats) {
                console.log(stats?.toString());
            } else {
                console.log('Compile done!');
            }
            if (!stats.hasErrors()) {
                this.wsServer.clients.forEach((client) => {
                    if (client.readyState === ws.OPEN) {
                        client.send('reload');
                    }
                });
            }
        });
    }

    serveIndex(res) {
        let contents = fs.readFileSync(path.join(this.config.output.path, 'index.html'), 'utf8');
        // insert the hook
        contents += `
        <script>
            function __connectToWebsocket() {
                const socket = new WebSocket('ws://localhost:8080');
                socket.addEventListener('message', (event) => {
                    window.location.reload();
                });

                socket.addEventListener('close', () => {
                    //console.error('builder socket closed, attempting to reconnect');
                    setTimeout(__connectToWebsocket, 1000);
                });
            }
            __connectToWebsocket();
        </script>`;
        res.contentType('text/html');
        res.write(contents);
        res.end();
    }

    serveStatic(req, res) {
        const buildPath = path.resolve(this.config.output.path, req.url.substr(1));
        if (fs.existsSync(buildPath)) {
            sendFile(res, buildPath);
        } else {
            res.status(404);
            res.end();
        }
    }

    setServer(server) {
        server.on('upgrade', (request, socket, head) => {
            this.wsServer.handleUpgrade(request, socket, head, socket => {
                this.wsServer.emit('connection', socket, request);
            });
        });
    }

    // serve the build files no matter how long the url is. This only works with
    // a flat build directory
    serve(req, res) {
        const urlPath = req.url;
        const urlPathList = urlPath.split("/");
        let file = urlPathList[urlPathList.length-1];
        if (file === '') {
            file = 'index.html';
        }

        if (file === 'index.html') {
            this.serveIndex(res);
            return
        }

        const buildPath = path.resolve(this.config.output.path, file);
        if (fs.existsSync(buildPath)) {
            sendFile(res, buildPath);
        } else {
            res.status(404);
            res.end();
        }
    }
}

module.exports = Builder;